# REACT CHALLENGE

Welcome to React Front-End Challenge

This challenge serves to evaluate your level of proficiency in React and your capacity to solve a day-to-day problem.

In this example you will find a **working** HTML5 sound player that has been implemented using HTML / CSS3 / jQuery.
Your job is to **re-implement it** using **React**.

Take a look at the sound player:

https://codepen.io/felipevega/pen/ZqqBxJ

#### TODO

1. Implement the same sound player using React. Try to preserve as much as possible the animations and interactions from the original sound player.
2. Replace the hardcoded values (albums, track anmes, track urls) by a request to a server using [fetch](https://github.github.io/fetch/) or [react-refetch](https://github.com/heroku/react-refetch). Tip: To save you some time, use a fake JSON service (https://fakejson.com/)
3. Make the components look great with CSS (BONUS: Show us your skills using [flexbox](https://www.youtube.com/watch?v=Vj7NZ6FiQvo&list=PLu8EoSxDXHP7xj_y6NIAhy0wuCd4uVdid)).

#### KICK OFF

You just need to clone this repo, create a React app and start working on it.

```sh
npx create-react-app react-sound-player
cd react-sound-player
npm start
```

For further information about how to create a React app please visit [Create React app](https://github.com/facebook/create-react-app).

##### TUTORIALS
- [Create React App](https://github.com/facebook/create-react-app).
- [ES2015 Tutorial](https://babeljs.io/docs/learn-es2015/)
- [What The Flexbox](https://www.youtube.com/watch?v=Vj7NZ6FiQvo&list=PLu8EoSxDXHP7xj_y6NIAhy0wuCd4uVdid)

##### WHEN DONE

After you've finish, just send us an email letting us know that you're ready to demo it and we'll take it from there.